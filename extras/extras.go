package extras

import (
	"net"
	"reflect"
	"unsafe"
	_ "unsafe"

	"codeberg.org/gruf/go-mangler"
)

func init() {
	// Register standard library performant manglers.
	mangler.Register(reflect.TypeOf(net.IPAddr{}), mangle_ipaddr)
	mangler.Register(reflect.TypeOf(&net.IPAddr{}), mangle_ipaddr_ptr)
}

func mangle_ipaddr(buf []byte, ptr unsafe.Pointer) []byte {
	i := *(*net.IPAddr)(ptr)
	buf = append(buf, i.IP...)
	buf = append(buf, i.Zone...)
	return buf
}

func mangle_ipaddr_ptr(buf []byte, ptr unsafe.Pointer) []byte {
	if ptr := (*net.IPAddr)(ptr); ptr != nil {
		buf = append(buf, '1')
		buf = append(buf, ptr.IP...)
		buf = append(buf, ptr.Zone...)
		return buf
	}
	buf = append(buf, '0')
	return buf
}
