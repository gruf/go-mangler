package mangler_test

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"reflect"
	"runtime/debug"
	"testing"
	"time"
	"unsafe"

	"codeberg.org/gruf/go-loosy"
	"codeberg.org/gruf/go-mangler"

	"github.com/cnf/structhash"
	"github.com/fxamacker/cbor"
	"github.com/mitchellh/hashstructure/v2"
)

type unsignedint uint

type byteslice []byte

var structvalue = struct {
	Field1 string
	Field2 *int32
	Field3 *float32
	Field4 struct {
		Field4_1 string
		Field4_2 *string
	}
	Field5 *struct {
		Field5_1 string
		Field5_2 *string
	}
	Field6 struct{ Field6_1 *string }
}{
	Field1: "hello",
	Field2: ptr_to(int32(1)),
	Field3: ptr_to(float32(1)),
	Field4: struct {
		Field4_1 string
		Field4_2 *string
	}{
		Field4_1: "hello",
		Field4_2: ptr_to("world"),
	},
	Field5: &struct {
		Field5_1 string
		Field5_2 *string
	}{
		Field5_1: "hello",
		Field5_2: ptr_to("world"),
	},
	Field6: struct{ Field6_1 *string }{
		ptr_to("onlyone"),
	},
}

var vars = []any{
	"hello world",
	[]byte("hello world"),
	[]rune("hello world"),
	byteslice("hello world"),
	[]int64{0, 1, 2, 3, 4, 5},
	[]uint32{0, 1, 2, 3, 4, 5},
	[]uintptr{0, 1, 2, 3, 4, 5},
	[]*uint{ptr_to(uint(0)), nil, ptr_to(uint(1)), nil, nil, nil},
	[]**uint{ptr_to(ptr_to(uint(0))), ptr_to(ptr_to(uint(1))), nil, nil, nil, nil},
	[6]int64{0, 1, 2, 3, 4, 5},
	[6]uint32{0, 1, 2, 3, 4, 5},
	[6]uintptr{0, 1, 2, 3, 4, 5},
	[6]byte{'h', 'e', 'l', 'l', 'o', '!'},
	[6]*uint{ptr_to(uint(0)), ptr_to(uint(1)), nil, nil, nil, nil},
	[6]**uint{ptr_to(ptr_to(uint(0))), ptr_to(ptr_to(uint(1))), nil, nil, nil, nil},
	int(1),
	int8(2),
	int16(3),
	int32(4),
	int64(5),
	uint(6),
	uint8(7),
	uint16(8),
	uint32(9),
	uint64(10),
	uintptr(11),
	float32(12),
	float64(13),
	complex64(14),
	complex128(15),
	unsignedint(16),
	time.Now(),
	time.Second,
	structvalue,
	func() url.URL { u, _ := url.Parse("https://google.com/"); return *u }(),
	ptr_to("hello world"),
	ptr_to([]byte("hello world")),
	ptr_to([]rune("hello world")),
	ptr_to(byteslice("hello world")),
	ptr_to([]int64{0, 1, 2, 3, 4, 5}),
	ptr_to([]uint32{0, 1, 2, 3, 4, 5}),
	ptr_to([]uintptr{0, 1, 2, 3, 4, 5}),
	ptr_to([]*uint{ptr_to(uint(0)), ptr_to(uint(1)), nil, nil, nil, nil}),
	ptr_to([]**uint{ptr_to(ptr_to(uint(0))), ptr_to(ptr_to(uint(1))), nil, nil, nil, nil}),
	ptr_to([6]int64{0, 1, 2, 3, 4, 5}),
	ptr_to([6]uint32{0, 1, 2, 3, 4, 5}),
	ptr_to([6]uintptr{0, 1, 2, 3, 4, 5}),
	ptr_to([6]byte{'h', 'e', 'l', 'l', 'o', '!'}),
	ptr_to([6]*uint{ptr_to(uint(0)), ptr_to(uint(1)), nil, nil, nil, nil}),
	ptr_to([6]**uint{ptr_to(ptr_to(uint(0))), ptr_to(ptr_to(uint(1))), nil, nil, nil, nil}),
	ptr_to(int(1)),
	ptr_to(int8(2)),
	ptr_to(int16(3)),
	ptr_to(int32(4)),
	ptr_to(int64(5)),
	ptr_to(uint(6)),
	ptr_to(uint8(7)),
	ptr_to(uint16(8)),
	ptr_to(uint32(9)),
	ptr_to(uint64(10)),
	ptr_to(uintptr(11)),
	ptr_to(float32(12)),
	ptr_to(float64(13)),
	ptr_to(complex64(14)),
	ptr_to(complex128(15)),
	ptr_to(unsignedint(16)),
	ptr_to(time.Now()),
	ptr_to(time.Second),
	ptr_to(structvalue),
	func() *url.URL { u, _ := url.Parse("https://google.com/"); return u }(),

	// All of the below cause
	// "binary" benchmark to fail:
	(*string)(nil),
	(*[]byte)(nil),
	(*[]rune)(nil),
	(*byteslice)(nil),
	(*[]int64)(nil),
	(*[]uint32)(nil),
	(*[]uintptr)(nil),
	(*int)(nil),
	(*int8)(nil),
	(*int16)(nil),
	(*int32)(nil),
	(*int64)(nil),
	(*uint)(nil),
	(*uint8)(nil),
	(*uint16)(nil),
	(*uint32)(nil),
	(*uint64)(nil),
	(*uintptr)(nil),
	(*float32)(nil),
	(*float64)(nil),
	(*complex64)(nil),
	(*complex128)(nil),
	(*unsignedint)(nil),
	(*time.Time)(nil),
	(*time.Duration)(nil),
}

func ptr_to[T any](t T) *T {
	return &t
}

func TestMangleIntUniqueness(t *testing.T) {
	test := map[string]bool{}
	for i := uint(0); i < 1e6; i++ {
		key := mangler.String(i)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
	}
}

func TestMangleInt8Uniqueness(t *testing.T) {
	test := map[string]bool{}
	for i := uint8(0); i < 255; i++ {
		key := mangler.String(i)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
	}
}

func TestMangleInt16Uniqueness(t *testing.T) {
	test := map[string]bool{}
	for i := uint16(0); i < ^uint16(0); i++ {
		key := mangler.String(i)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
	}
}

func TestMangleInt32Uniqueness(t *testing.T) {
	test := map[string]bool{}
	for i := uint32(0); i < 1e6; i++ {
		key := mangler.String(i)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
	}
}

func TestMangleInt64Uniqueness(t *testing.T) {
	test := map[string]bool{}
	for i := uint64(0); i < 1e6; i++ {
		key := mangler.String(i)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
	}
}

func TestMangleFloat32Uniqueness(t *testing.T) {
	test := map[string]bool{}
	for i := float32(0); i < 1e6; i++ {
		key := mangler.String(i)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
	}
}

func TestMangleFloat64Uniqueness(t *testing.T) {
	test := map[string]bool{}
	for i := float64(0); i < 1e6; i++ {
		key := mangler.String(i)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
	}
}

func TestMangleTimeUniqueness(t *testing.T) {
	test := map[string]bool{}
	var tm time.Time
	for i := 0; i < 1e6; i++ {
		key := mangler.String(tm)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
		tm = tm.Add(time.Second)
	}
}

func TestMangleDurationUniqueness(t *testing.T) {
	test := map[string]bool{}
	var d time.Duration
	for i := 0; i < 1e6; i++ {
		key := mangler.String(d)
		if test[key] {
			t.Fatal("uniqueness fail")
		}
		test[key] = true
		d += time.Second
	}
}

func TestMangleStructs(t *testing.T) {
	var v any
	defer func() {
		if r := recover(); r != nil {
			const msg = "recovered panic mangling %T: %v\n\n%s\n"
			fmt.Fprintf(os.Stderr, msg, v, r, debug.Stack())
		}
	}()
	m := make(map[string]struct{})
	buf := make([]byte, 0)
	for _, v = range []any{
		struct{ onlyone string }{},
		struct{ onlyone string }{"hello"},
		struct{ onlyone *string }{},
		struct{ onlyone *string }{new(string)},
		struct{ onlyone *string }{ptr_to("hello")},
		struct{ onlyone [2]string }{},
		struct{ onlyone [2]string }{[2]string{"hello", "world"}},
		struct{ onlyone [2]*string }{},
		struct{ onlyone [2]*string }{[2]*string{new(string), new(string)}},
		struct{ onlyone [2]*string }{[2]*string{ptr_to("hello"), ptr_to("world")}},
		struct{ nested struct{ onlyone string } }{},
		struct{ nested struct{ onlyone string } }{struct{ onlyone string }{"hello"}},
		struct{ nested struct{ onlyone *string } }{},
		struct{ nested struct{ onlyone *string } }{struct{ onlyone *string }{new(string)}},
		struct{ nested struct{ onlyone *string } }{struct{ onlyone *string }{ptr_to("hello")}},
		struct{ nested *struct{ onlyone string } }{},
		struct{ nested *struct{ onlyone string } }{&struct{ onlyone string }{}},
		struct{ nested *struct{ onlyone string } }{&struct{ onlyone string }{"hello"}},
		struct{ nested *struct{ onlyone *string } }{},
		struct{ nested *struct{ onlyone *string } }{&struct{ onlyone *string }{}},
		struct{ nested *struct{ onlyone *string } }{&struct{ onlyone *string }{new(string)}},
		struct{ nested *struct{ onlyone *string } }{&struct{ onlyone *string }{ptr_to("hello")}},
		[2]struct{ onlyone string }{},
		[2]struct{ onlyone string }{{"hello"}, {"world"}},
		[2]struct{ onlyone *string }{},
		[2]struct{ onlyone *string }{{new(string)}, {new(string)}},
		[2]struct{ onlyone *string }{{ptr_to("hello")}, {ptr_to("world")}},
		[2]*struct{ onlyone string }{},
		[2]*struct{ onlyone string }{{"hello"}, {"world"}},
		[2]*struct{ onlyone *string }{},
		[2]*struct{ onlyone *string }{{new(string)}, {new(string)}},
		[2]*struct{ onlyone *string }{{ptr_to("hello")}, {ptr_to("world")}},
		struct {
			just *string
			two  *int
		}{},
		struct {
			just *string
			two  *int
		}{new(string), new(int)},
		struct {
			just *string
			two  *int
		}{ptr_to("hello"), ptr_to(69)},
		ptr_to(struct {
			just *string
			two  *int
		}{}),
		ptr_to(struct {
			just *string
			two  *int
		}{new(string), new(int)}),
		ptr_to(struct {
			just *string
			two  *int
		}{ptr_to("hello"), ptr_to(69)}),
		ptr_to(struct{ onlyone string }{}),
		ptr_to(struct{ onlyone string }{"hello"}),
		ptr_to(struct{ onlyone *string }{}),
		ptr_to(struct{ onlyone *string }{new(string)}),
		ptr_to(struct{ onlyone *string }{ptr_to("hello")}),
		ptr_to([2]struct{ onlyone string }{}),
		ptr_to([2]struct{ onlyone string }{{"hello"}, {"world"}}),
		ptr_to([2]struct{ onlyone *string }{}),
		ptr_to([2]struct{ onlyone *string }{{new(string)}, {new(string)}}),
		ptr_to([2]struct{ onlyone *string }{{ptr_to("hello")}, {ptr_to("world")}}),
		ptr_to(ptr_to(struct{ onlyone string }{})),
		ptr_to(ptr_to(struct{ onlyone string }{"hello"})),
		ptr_to(ptr_to(struct{ onlyone *string }{})),
		ptr_to(ptr_to(struct{ onlyone *string }{new(string)})),
		ptr_to(ptr_to(struct{ onlyone *string }{ptr_to("hello")})),
		ptr_to(struct{ nested struct{ onlyone string } }{}),
		ptr_to(struct{ nested struct{ onlyone string } }{struct{ onlyone string }{"hello"}}),
		ptr_to(struct{ nested struct{ onlyone *string } }{}),
		ptr_to(struct{ nested struct{ onlyone *string } }{struct{ onlyone *string }{new(string)}}),
		ptr_to(struct{ nested struct{ onlyone *string } }{struct{ onlyone *string }{ptr_to("hello")}}),
		ptr_to(struct{ nested *struct{ onlyone string } }{}),
		ptr_to(struct{ nested *struct{ onlyone string } }{&struct{ onlyone string }{}}),
		ptr_to(struct{ nested *struct{ onlyone string } }{&struct{ onlyone string }{"hello"}}),
		ptr_to(struct{ nested *struct{ onlyone *string } }{}),
		ptr_to(struct{ nested *struct{ onlyone *string } }{&struct{ onlyone *string }{}}),
		ptr_to(struct{ nested *struct{ onlyone *string } }{&struct{ onlyone *string }{new(string)}}),
		ptr_to(struct{ nested *struct{ onlyone *string } }{&struct{ onlyone *string }{ptr_to("hello")}}),
		ptr_to(ptr_to([2]struct{ onlyone string }{})),
		ptr_to(ptr_to([2]struct{ onlyone string }{{"hello"}, {"world"}})),
		ptr_to(ptr_to([2]struct{ onlyone *string }{})),
		ptr_to(ptr_to([2]struct{ onlyone *string }{{new(string)}, {new(string)}})),
		ptr_to(ptr_to([2]struct{ onlyone *string }{{ptr_to("hello")}, {ptr_to("world")}})),
	} {
		fmt.Println(reflect.TypeOf(v))
		buf = mangler.Append(buf[:0], v)
		str := string(buf)
		fmt.Printf("==> %q\n", str)
		if _, ok := m[str]; ok {
			t.Fatal("non unique struct")
		}
		m[str] = struct{}{}
	}
}

func TestMangleVarious(t *testing.T) {
	var v any
	defer func() {
		if r := recover(); r != nil {
			const msg = "recovered panic mangling %T: %v\n\n%s\n"
			fmt.Fprintf(os.Stderr, msg, v, r, debug.Stack())
		}
	}()
	buf := make([]byte, 0)
	for _, v = range vars {
		fmt.Println(reflect.TypeOf(v))
		buf = mangler.Append(buf[:0], v)
	}
}

func BenchmarkMangle(b *testing.B) {
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var b []byte

		for pb.Next() {
			for _, v := range vars {
				b = mangler.Append(b, v)
				b = b[:0]
			}
		}

		b = b[:0]
	})
}

func BenchmarkMangleKnown(b *testing.B) {
	var manglers []mangler.Mangler

	for _, v := range vars {
		t := reflect.TypeOf(v)
		m := mangler.Get(t)
		manglers = append(manglers, m)
	}

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var b []byte

		for pb.Next() {
			for i, v := range vars {
				p := eface_data(v)
				b = manglers[i](b, p)
				b = b[:0]
			}
		}

		b = b[:0]
	})
}

func BenchmarkJSON(b *testing.B) {
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var b []byte

			for _, v := range vars {
				b, _ = json.Marshal(v)
				b = b[:0]
			}

			b = b[:0]
		}
	})
}

func BenchmarkLoosy(b *testing.B) {
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var b bytes.Buffer

		for pb.Next() {
			for _, v := range vars {
				loosy.MarshalWrite(&b, v)
				b.Reset()
			}
		}

		b.Reset()
	})
}

// func BenchmarkBinary(b *testing.B) {
// 	b.ResetTimer()
// 	b.RunParallel(func(pb *testing.PB) {
// 		var b bytes.Buffer

// 		for pb.Next() {
// 			for _, v := range vars {
// 				binary.Write(&b, binary.LittleEndian, v)
// 				b.Reset()
// 			}
// 		}

// 		b.Reset()
// 	})
// }

func BenchmarkFmt(b *testing.B) {
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var b []byte

		for pb.Next() {
			for _, v := range vars {
				b = fmt.Append(b, v)
				b = b[:0]
			}
		}

		b = b[:0]
	})
}

func BenchmarkFxmackerCbor(b *testing.B) {
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		opts := cbor.CanonicalEncOptions()

		for pb.Next() {
			var b []byte

			for _, v := range vars {
				b, _ = cbor.Marshal(v, opts)
			}

			b = b[:0]
		}
	})
}

func BenchmarkMitchellhHashStructure(b *testing.B) {
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var b []byte

			for _, v := range vars {
				u, _ := hashstructure.Hash(v, hashstructure.FormatV2, nil)
				b = binary.LittleEndian.AppendUint64(b, u)
				b = b[:0]
			}

			b = b[:0]
		}
	})
}

func BenchmarkCnfStructhash(b *testing.B) {
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var b []byte

			for _, v := range vars {
				b = structhash.Dump(v, 0)
				b = b[:0]
			}

			b = b[:0]
		}
	})
}

func eface_data(a any) unsafe.Pointer {
	type eface struct{ _, data unsafe.Pointer }
	return (*eface)(unsafe.Pointer(&a)).data
}
