module codeberg.org/gruf/go-mangler

go 1.19

require (
	codeberg.org/gruf/go-loosy v0.0.0-20231007123304-bb910d1ab5c4
	github.com/cnf/structhash v0.0.0-20201127153200-e1b16c1ebc08
	github.com/fxamacker/cbor v1.5.1
	github.com/mitchellh/hashstructure/v2 v2.0.2
)

require (
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/x448/float16 v0.8.4 // indirect
)
